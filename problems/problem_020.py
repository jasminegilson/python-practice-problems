# Complete the has_quorum function to test to see if the number
# of the people in the attendees list is greater than or equal
# to 50% of the number of people in the members list.


def has_quorum(members, attendees):
    return len(attendees) >= len(members) / 2

print(has_quorum("23", "79"))
