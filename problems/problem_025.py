# Complete the calculate_sum function which accepts
# a list of numerical values and returns the sum of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#

def calculate_sum(values):
    if not values:
        return None
    return sum(values)


values = [2, 4, 6, 8]
result = calculate_sum(values)
print("The sum of {} is {}.".format(values, result))
