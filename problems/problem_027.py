# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
#

def max_in_list(lst):
    if not lst:
        return None
    else:
        return max(lst)

print(max_in_list([1, 2, 3, 4, 5]))
