# Complete the calculate_average function which accepts
# a list of numerical values and returns the average of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#
# Pseudocode is available for you

def calculate_average(nums):
    if not nums:
        return None
    return sum(nums) / len(nums)

print(calculate_average([1, 2, 3, 4, 5])) # Output: 3.0
print(calculate_average([])) # Output: None
