# Complete the max_of_three function so that returns the
# maximum of three values.
#
# If two values are the same maximum value, return either of
# them.
# If the all of the values are the same, return any of them
#
# Use the >= operator for greater than or equal to

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.


def max_of_three(value1, value2, value3):
    pass

    if (value1 >= value2) and (value1 >= 3):
        largest = value1
    elif (value2 >= value1) and (value2 >= value3):
        largest = value2
    else:
        largest = value3

    return largest


# Driven code
a = 5
b = 20
c = 6

print(max_of_three(1, 2, 3))
