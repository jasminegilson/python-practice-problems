# Write four classes that meet these requirements.
#
# Name:       Animal
class Animal:
    def __init__(self, number_of_legs, primary_color):
        self.number_of_legs = number_of_legs
        self.primary_color = primary_color
# Required state:
#    * number_of_legs, the number of legs the animal has
#    * primary_color, the primary color of the animal
#
# Behavior:

    def describe(self):
        return (
            self.__class__.__name__,
            + " has "
            + str(self.number_of_legs),
            + " legs and is primarily ",
            +self.primary_color
        )

#    * describe()       # Returns a string that describes that animal
#                         in the format
#                                self.__class__.__name__
#                                + " has "
#                                + str(self.number_of_legs)
#                                + " legs and is primarily "
#                                + self.primary_color
#
#
# Name:       Dog, inherits from Animal


class Dog(Animal):
    def speak():
        return "Bark!"


# Required state:       inherited from Animal
# Behavior:
#    * speak()          # Returns the string "Bark!"
#
#
#
# Name:       Cat, inherits from Animal


    class Cat(Animal):
        def speak():
            return "Miao!"
# Required state:       inherited from Animal
#
# Behavior:
#    * speak()          # Returns the string "Miao!"
#
#
#
# Name:       Snake, inherits from Animal
#
# Required state:       inherited from Animal
#
# Behavior:
#    * speak()          # Returns the string "Sssssss!"


class Snake(Animal):
    def speak():
        return "Sssssss!"


animal = Animal("number_of_legs: Any", "primary_color: Any")

print(animal.describe())
