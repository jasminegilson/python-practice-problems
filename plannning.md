# Planning

## Research

* [ ] Vocab
* [ ] Functions
* [ ] Methods

## Problem decomposition

* [ ] Input
* [ ] Output
* [ ] Examples
* [ ] Conditions (if)
* [ ] Iteration (loop)

## Problems

### 01 minimum_value
# Complete the minimum_value function so that returns the
# minimum of two values.
#
# If the values are the same, return either.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

Vocab: def, value, return, if, else print



def minimum_value(value1, value2):
    if value1 < value2:
        return value1
    else:
        return value2

print(minimum_value(1, 2))


### 04 max_of_three
max(3)
return max(3)

if value2 = max(3)
    return either

elif 3 >= 3
    return either

Solution
def max_of_three(value1, value2, value3):
    pass

    if (value1 >= value2) and (value1 >= 3):
        largest = value1
    elif (value2 >= value1) and (value2 >= value3):
        largest = value2
    else:
        largest = value3

    return largest

# Driven code
a = 10
b = 14
c = 12

print(max_of_three(1, 2, 3))




### 06 can_skydive

### 09 palindrome
Input: Racecar
Output:Yes

Input: Malibu
Output: No
def, return, reverse


## 10 divisib;e_fizz
test num%3 == 3
